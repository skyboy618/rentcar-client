import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CarComponent } from './car/car.component';
import { from } from 'rxjs';
import { LocationComponent } from './location/location.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { CreateCarComponent } from './create-car/create-car.component';
import { FormsModule } from '@angular/forms';
import { UpdateCarComponent } from './update-car/update-car.component';
import { UpdateLocationComponent } from './update-location/update-location.component';
import { CreateLocationComponent } from './create-location/create-location.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { HeaderComponent } from './header/header.component';
import { DataTablesModule } from 'angular-datatables';
import {Ng2SearchPipeModule } from 'ng2-search-filter';
import {Ng2OrderModule} from 'ng2-order-pipe';
import { NgxPaginationModule } from 'ngx-pagination';




@NgModule({
  declarations: [
    AppComponent,
    CarComponent,
    LocationComponent,
    AboutComponent,
    HomeComponent,
    CreateCarComponent,
    UpdateCarComponent,
    UpdateLocationComponent,
    CreateLocationComponent,
    LoginComponent,
    LogoutComponent,
    HeaderComponent
    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    Ng2OrderModule,
    Ng2SearchPipeModule,
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
