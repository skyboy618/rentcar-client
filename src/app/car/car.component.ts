import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Car } from '../car';
import { CarService } from '../car.service';
import { Location } from '../location';
import { LocationService } from '../location.service';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css'],

})
export class CarComponent implements OnInit {

cars : Car[];
brand : any;
p: number = 1;




// location : Location;

  constructor(private carService : CarService, private locationService : LocationService,
    private router: Router) { }

  ngOnInit(): void {
   this.getCars();
  
  }

 

private getCars(){
  this.carService.getCarList().subscribe( data => {
    this.cars = data;
  
  });
}


updateCar(id: number){
this.router.navigate(['update-car', id]);
}


deleteCar(id : number){
  this.carService.deleteCar(id).subscribe( data =>{
    console.log( "succesfully  deleted");
    this.getCars();
  })
}

goToCreateCar(){
  this.router.navigate(['create-car']);
}


Search(){
  if(this.brand == ""){
    this.ngOnInit();
  }else{
    this.cars = this.cars.filter(res =>{
      return res.brand.toLocaleLowerCase().match(this.brand.toLocaleLowerCase());
    })
  }
}

key: string =  'id';
reverse : boolean = false;
sort(key){
  this.key = key;
  this.reverse = !this.reverse;
}



}
