import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '../location';
import { LocationService} from '../location.service';



@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit {

  locations : Location[];

  constructor(private locationService: LocationService,
    private router: Router) { }

  ngOnInit(): void {
    this.getLocations();
  }

 
  private getLocations(){
    this.locationService.getLocationList().subscribe( data => {
      this.locations = data;
    });
  }


  updateLocation(id: number){
    this.router.navigate(['update-location', id]);
    }
    
    
    deleteLocation(id : number){
      this.locationService.deleteLocation(id).subscribe( data =>{
        console.log( "succesfully  deleted");
        this.getLocations();
      })
    }

    goToCreateLocation(){ 
      this.router.navigate(['create-location']);
    }
}
