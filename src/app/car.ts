import { Location } from "./location";
// import {Observable} from 'rxjs';

export class Car {

    id : number;
    brand : string;
    model : string;
    color : string;
    fabricationYear : number;
    isAvailable : boolean;
    pricePerDay : number;
    location : Location;
    // photo: Observable<any>;

   
}
