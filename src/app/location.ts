import { Car } from "./car";

export class Location {

    id : number;
    address : string;
    cars : Car[];
}
