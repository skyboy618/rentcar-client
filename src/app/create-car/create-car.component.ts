
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Car } from '../car';
import { CarService } from '../car.service';
import { LocationService } from '../location.service';

@Component({
  selector: 'app-create-car',
  templateUrl: './create-car.component.html',
  styleUrls: ['./create-car.component.css']
})
export class CreateCarComponent implements OnInit {

  car : Car = new Car();
  locations : Location[] = [];
  constructor(private carService : CarService, private locationService : LocationService,
     private router : Router ) { }

  ngOnInit(): void {
  }



saveCar(){
  this.carService.addCar(this.car).subscribe( data =>{
    console.log(data);
  
   
  },
  error => console.log(error));
}

goToCarList(){
 
  this.router.navigate(['/cars']);
  
}


onSubmit(){
 console.log(this.car);
  this.saveCar();
  this.goToCarList();
 
 
}

}
