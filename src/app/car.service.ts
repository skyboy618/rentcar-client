import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Car } from './car';
import { from } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CarService {

private baseUrl = "http://localhost:8080/api/car";

  constructor(private httpClient : HttpClient) { }

    getCarList(): Observable<Car[]> {
        return this.httpClient.get<Car[]>(this.baseUrl);
    }

    addCar(car : Car):Observable<Object>{
      return this.httpClient.post(this.baseUrl +'/add', car);
    }

    getCarById(id: number):Observable<Car>{
      return this.httpClient.get<Car>(`${this.baseUrl}/get/${id}`);
    }

    updateCar(id:number, car : Car):Observable<Object>{
      return this.httpClient.put(`${this.baseUrl}/update/${id}`, car);
    }   

    
  deleteCar(id: number): Observable<Object>{
    return this.httpClient.delete(`${this.baseUrl}/delete/${id}`);
  }

  getLocationFromCar(id : number) : Observable<any>{
    return this.httpClient.get(`${this.baseUrl}/getAddress/${id}`);
  }
}
