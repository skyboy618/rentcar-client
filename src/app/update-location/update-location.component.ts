import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '../location';
import { LocationService } from '../location.service';

@Component({
  selector: 'app-update-location',
  templateUrl: './update-location.component.html',
  styleUrls: ['./update-location.component.css']
})
export class UpdateLocationComponent implements OnInit {

  id : number;
location : Location = new Location();

  constructor(private locationService : LocationService,
    private router : Router,
    private route : ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.locationService.getLocationById(this.id).subscribe(data=>{
      this.location = data;
    },  error => console.log(error));
  }


  updateCar(){
    this.locationService.updateLocation(this.id, this.location)
    .subscribe(data => {
      console.log(data);
      this.location = new Location;
      this.goToLocationsList();
    }, error => console.log(error));
  }


  onSubmit(){
    this.locationService.updateLocation(this.id, this.location).subscribe(data =>{
      this.goToLocationsList();
    },error => console.log(error));
  }

  goToLocationsList(){
    this.router.navigate(['/locations']);
  }

}
