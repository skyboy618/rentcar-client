import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Location } from './location';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  private baseUrl = "http://localhost:8080/api/warehouse";

  constructor(private httpClient : HttpClient) { }



  getLocationList(): Observable<Location[]> {
    return this.httpClient.get<Location[]>(this.baseUrl);
}

addLocation(location : Location):Observable<Object>{
  return this.httpClient.post(this.baseUrl +'/save', location);
}

getLocationById(id: number):Observable<Location>{
  return this.httpClient.get<Location>(`${this.baseUrl}/get/${id}`);
}

updateLocation(id:number, location : Location):Observable<Object>{
  return this.httpClient.put(`${this.baseUrl}/update/${id}`, location);
}   


deleteLocation(id: number): Observable<Object>{
return this.httpClient.delete(`${this.baseUrl}/delete/${id}`);
}

}
