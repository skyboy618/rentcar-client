import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { CarComponent } from './car/car.component';
import { CreateCarComponent } from './create-car/create-car.component';
import { CreateLocationComponent } from './create-location/create-location.component';
import { HomeComponent } from './home/home.component';
import { LocationComponent } from './location/location.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { UpdateCarComponent } from './update-car/update-car.component';
import { UpdateLocationComponent } from './update-location/update-location.component';
import { AuthGuardService } from './service/auth-guard.service';

const routes: Routes = [
  { path: 'cars' , component : CarComponent, canActivate:[AuthGuardService] },
  { path : 'locations' , component: LocationComponent, canActivate:[AuthGuardService] },
  { path : 'about', component: AboutComponent},
  { path : '', component : HomeComponent, canActivate:[AuthGuardService] },
  { path : 'create-car', component : CreateCarComponent, canActivate:[AuthGuardService] },
  { path : 'update-car/:id', component: UpdateCarComponent, canActivate:[AuthGuardService] },
  { path : 'update-location/:id', component:UpdateLocationComponent, canActivate:[AuthGuardService] },
  { path : 'create-location', component: CreateLocationComponent, canActivate:[AuthGuardService] },
  { path : 'login', component: LoginComponent},
  { path : 'logout', component: LogoutComponent, canActivate:[AuthGuardService] }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
