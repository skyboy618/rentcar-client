import { getAttrsForDirectiveMatching } from '@angular/compiler/src/render3/view/util';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import { AuthenticationService } from '../service/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username  = 'admin';
  password = '';
  invalidLogin = false;

  constructor(private router : Router, private loginService : AuthenticationService) { }

  ngOnInit(): void {
  }

  checkLogin(){
    if(this.loginService.authenticate(this.username, this.password)){
      this.router.navigate(['']);
      this.invalidLogin = false;
    }else{
      this.invalidLogin = true;
      alert('Please check your username and password and try again')
    }
  }

}
